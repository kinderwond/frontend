import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Paginate from 'vuejs-paginate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import InputMask from 'vue-input-mask';
import "./assets/css/style.css";
 
Vue.config.productionTip = false

Vue.component('input-mask', InputMask)
Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = `http://127.0.0.1:8089/api/`;
Vue.axios.defaults.headers.common["Content-Type"] = "application/x-www-form-urlencoded"
Vue.axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";

Vue.component('paginate', Paginate)

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
