import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    bucket: !localStorage.getItem('bucket') ? {} : JSON.parse(localStorage.getItem('bucket')),
    bucketStatus: true,
    commonPrice: 0,
    carts: [],
    isLoadMore: true,
    paginationConfig: {
      count: 10,
      max: 10
    }

  },
  getters: {
    getBucketSatus: state => state.bucketStatus,
    getBucket: (state) => state.bucket,
    getCarts: (state) => state.carts,
    getLoadMoreStatus: (state) => state.isLoadMore
  },
  mutations: {
    INCREMENT_PAGE_SIZE(state) {
      state.paginationConfig.max += state.paginationConfig.count;
    },
    async GET_CARTS(state) {
      let { data } = await Vue.axios.post("/cart/get", {
        count: state.paginationConfig.max
      })
      if (data.error != undefined) return console.error(data.error)

      state.carts = data.carts
      if (data.carts < state.paginationConfig.count)
        state.isLoadMore = false
    },
    SET_NEW_CART(state, item) {
      let objId = item.id
      state.bucket[objId] = {
        title: item.title,
        price: item.price,
        count: item.count
      }
      localStorage.setItem('bucket', JSON.stringify(state.bucket))
      state.bucketStatus = !state.bucketStatus
    },
    INCCREMENT_COMMON_PRICE(state, price) {
      state.commonPrice += price
    },
    DECREMENT_COMMON_PRICE(state, price) {
      state.commonPrice -= price
    }
  },
  actions: {
    async getCarts({ commit }) {
      await commit("GET_CARTS")
    },
    async loadMore({ commit }) {
      await commit("INCREMENT_PAGE_SIZE")
      await commit("GET_CARTS")
    },
    setNewCart({ commit }, item) {
      commit("SET_NEW_CART", item)
    },
    incrementPrice({ commit }, price) {
      commit("INCREMENT_COMMON_PRICE")
    },
    decrementPrice({ commit }, price) {
      commit("DECREMENT_COMMON_PRICE")
    }
  },
  modules: {
  }
})
