import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    beforeEnter: (to, from, next) => {
      next({path: '/cart'})
    }
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import("@/components/Carts.vue")
  },
  {
    path: '/shipping',
    name: 'Shipping',
    component: () => import("@/components/Shipping.vue")
  },
]

const router = new VueRouter({
  routes
})

export default router
